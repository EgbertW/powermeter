import struct
import sys
import binascii
import time
from datetime import datetime, timezone
import pprint
import elasticsearch

from pymodbus.constants import Endian
from pymodbus.payload import BinaryPayloadDecoder
from pymodbus.payload import BinaryPayloadBuilder
from pymodbus.exceptions import ModbusIOException

from struct import *
from pymodbus.client.sync import ModbusTcpClient
from pymodbus.framer.rtu_framer import ModbusRtuFramer

import logging
logging.basicConfig()
log = logging.getLogger()
#log.setLevel(logging.DEBUG)

from config import es_auth, es_host

#host = '192.168.0.7'
host = '10.6.0.140'
port = 502

client = ModbusTcpClient(host, port)
client.connect()

# Regs for SDM120M
sdm120_regs = [
    # Symbol                     Reg#  Format
    ( 'voltage',                 0x00, '%6.2f' ), # Voltage [V]
    ( 'current',                 0x06, '%6.2f' ), # Current [A]
    ( 'p_active',                0x0c, '%6.0f' ), # Active Power ("Wirkleistung") [W]
    ( 'p_apparent',              0x12, '%6.0f' ), # Apparent Power ("Scheinl.") [W]
    ( 'p_reactive',              0x18, '%6.0f' ), # Reactive Power ("Blindl.") [W]
    ( 'power_factor',            0x1e, '%6.3f' ), # Power Factor   [1]

    #( 'cos_phi',                 0x24, '%6.1f' ), # cos(Phi)?      [1]

    ( 'frequency',               0x46, '%6.2f' ), # Line Frequency [Hz]

    ( 'p_import_active',         0x48, '%6.2f' ), # Import active energy
    ( 'p_export_active',         0x4a, '%6.2f' ), # Export active energy

    ( 'p_import_reactive',       0x4c, '%6.2f' ), # Import reactive energy
    ( 'p_export_reactive',       0x4e, '%6.2f' ), # Export reactive energy

    ( 'total_power_demand',      0x54, '%6.2f' ), # Total system power demand
    ( 'import_power_demand',     0x58, '%6.2f' ), # Import system power demand
    ( 'max_import_power_demand', 0x5a, '%6.2f' ), # Maximum import system power demand
    ( 'export_power_demand',     0x5c, '%6.2f' ), # Export system power demand
    ( 'max_export_power_demand', 0x5e, '%6.2f' ), # Maximum export system power demand

    ( 'current_demand',          0x0102, '%6.2f' ), # Export system power demand
    ( 'maximum_current_demand',  0x0108, '%6.2f' ), # Export system power demand
    ( 'total_active_energy',     0x0156, '%6.2f' ), # Export system power demand
    ( 'total_reactive_energy',   0x0158, '%6.2f' ), # Maximum export system power demand
]

# Regs for SDM630-Modbus
sdm630_regs = [
    # Symbol                     Reg#  Format
    ( 'voltage_1',               0x00, '%6.2f' ), # Voltage Phase 1 [V]
    ( 'voltage_2',               0x02, '%6.2f' ), # Voltage Phase 2 [V]
    ( 'voltage_3',               0x04, '%6.2f' ), # Voltage Phase 3 [V]
    ( 'current_1',               0x06, '%6.2f' ), # Current Phase 1 [A]
    ( 'current_2',               0x08, '%6.2f' ), # Current Phase 2 [A]
    ( 'current_3',               0x0a, '%6.2f' ), # Current Phase 3 [A]
    ( 'p_active_1',              0x0c, '%6.0f' ), # Active Power Phase 1 ("Wirkleistung") [W]
    ( 'p_active_2',              0x0e, '%6.0f' ), # Active Power Phase 2 ("Wirkleistung") [W]
    ( 'p_active_3',              0x10, '%6.0f' ), # Active Power Phase 3 ("Wirkleistung") [W]
    ( 'p_apparent_1',            0x12, '%6.0f' ), # Apparent Power Phase 1 [VAr]
    ( 'p_apparent_2',            0x14, '%6.0f' ), # Apparent Power Phase 2 [VAr]
    ( 'p_apparent_3',            0x16, '%6.0f' ), # Apparent Power Phase 3 [VAr]
    ( 'p_reactive_1',            0x18, '%6.0f' ), # Reactive Power Phase 1 [VAr]
    ( 'p_reactive_2',            0x1a, '%6.0f' ), # Reactive Power Phase 2 [VAr]
    ( 'p_reactive_3',            0x1c, '%6.0f' ), # Reactive Power Phase 3 [VAr]
    ( 'power_factor_1',          0x1e, '%6.0f' ), # Power Factor Phase 1 
    ( 'power_factor_2',          0x20, '%6.0f' ), # Power Factor Phase 2 
    ( 'power_factor_3',          0x22, '%6.0f' ), # Power Factor Phase 3 

    ( 'cos_phi_1',               0x24, '%6.1f' ), # Phase 1 Angle    [degrees]
    ( 'cos_phi_2',               0x26, '%6.1f' ), # Phase 2 Angle    [degrees]
    ( 'cos_phi_3',               0x28, '%6.1f' ), # Phase 3 Angle    [degrees]

    ( 'avg_line_voltage',        0x2a, '%6.2f' ), # Average Line to Neutral Volts
    ( 'avg_line_current',        0x2e, '%6.2f' ), # Average Line current [A]
    ( 'sum_line_currents',       0x30, '%6.2f' ), # Sum of Line Currents [A]

    ( 'total_system_power',          0x34, '%6.2f' ), # Total system power [W]
    ( 'total_system_apparent_power', 0x38, '%6.2f' ), # Total system apparent power [VA]
    ( 'total_system_reactive_power', 0x3c, '%6.2f' ), # Total system reactive power [VAr]
    ( 'total_system_power_factor',   0x3e, '%6.2f' ), # Total system power factor
    ( 'total_system_phase_angle',    0x42, '%6.2f' ), # Total system phase angle [Degrees]

    ( 'frequency',               0x46, '%6.2f' ), # Line Frequency [Hz]

    ( 'p_import_active',         0x48, '%6.2f' ), # Import active energy [kWh]
    ( 'p_export_active',         0x4a, '%6.2f' ), # Export active energy [kWh]

    ( 'p_import_reactive',       0x4c, '%6.2f' ), # Import reactive energy [VArh]
    ( 'p_export_reactive',       0x4e, '%6.2f' ), # Export reactive energy [VArh]

    ( 'p_total_apparent_power',  0x50, '%6.2f' ), # Total VAh apparent power [VAh]
    ( 'p_total_apparent_power',  0x52, '%6.2f' ), # Total Ah [Ah]

    ( 'total_power_demand',      0x54, '%6.2f' ), # Total system power demand
    ( 'import_power_demand',     0x58, '%6.2f' ), # Import system power demand
    ( 'max_import_power_demand', 0x5a, '%6.2f' ), # Maximum import system power demand
    ( 'export_power_demand',     0x5c, '%6.2f' ), # Export system power demand
    ( 'max_export_power_demand', 0x5e, '%6.2f' ), # Maximum export system power demand

    ( 'current_demand_1'    ,    0x0102, '%6.2f' ), # Export system power demand
    ( 'maximum_current_demant_1',0x0108, '%6.2f' ), # Export system power demand
]

sdm120_meters = [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 16]
sdm220_meters = [17]
sdm630_meters = [1, 15]

phase_fields = ['voltage', 'current', 'p_active', 'p_apparent', 'p_reactive', 'power_factor']
phase_mapping = {
        # ALS 1
        2: 3,
        3: 3,
        4: 3,
        5: 3,
        6: 3,

        # ALS 2
        7: 1,
        8: 1,
        9: 1,
        10: 1,
        
        # ALS 3
        11: 2,
        12: 2,
        13: 2,
        14: 2,

        # Alamat ZP
        16: 1,

        # Alamat Server
        17: 1
}

meters = sdm120_meters + sdm220_meters + sdm630_meters

if len(sys.argv) > 1:
    meters = [int(sys.argv[1])]

print("Polling meters: %s" % meters)

es = elasticsearch.Elasticsearch(["%s@%s" % (es_auth, es_host)], use_ssl=True, verify_certs=False)

def get_with_retry(client, register, address, attempts):
    if attempts <= 0:
        print("Aborting request to address %d with register %04x" % (address, register))
        return

    resp = client.read_input_registers(register, 0x2, unit=address)
    if isinstance(resp, ModbusIOException):
        print(repr(resp))
        print("Error while reading meter %d at register %04x (for :%s): %s" % (address, reg[1], reg[0], resp.string))
        return get_with_retry(client, register, address, attempts - 1)

    return resp

last = 0
TIME_INTERVAL = 150
next_time = ((int(round(time.time())) // TIME_INTERVAL) * TIME_INTERVAL) + TIME_INTERVAL
print("Next run at %s" % next_time)

while True:
    cur = time.time()
    rem = next_time - cur
    if rem >= 0:
        print("Awaiting %f seconds" % rem)
        time.sleep(rem)

    start = next_time
    serie_timestamp = datetime.utcfromtimestamp(start).replace(tzinfo=timezone.utc).isoformat()
    print("Start reading at %s" % serie_timestamp)
    next_time = start + TIME_INTERVAL
    print("Next run at %s" % next_time)

    for address in meters:
        meter_data = {}
        tp = ""
        if address in sdm120_meters:
            regs = sdm120_regs
            tp = "SDM120M meter"
        elif address in sdm220_meters:
            regs = sdm120_regs
            tp = "SDM220M meter"
        elif address in sdm630_meters:
            regs = sdm630_regs
            tp = "SDM630-Modbus meter"
        else:
            print("I don't know meter %d" % address)

        print("Loading data for %s meter: %d" % (tp, address))
        for reg in regs:
            #rr = client.read_input_registers(reg[1], 0x2, unit=address)
            rr = get_with_retry(client, reg[1], address, 3)
            time.sleep(0.05)

            if isinstance(rr, ModbusIOException):
                print("Error while reading meter %d at register %04x (for :%s): %s" % (address, reg[1], reg[0], rr.string))
                continue

            float_val = struct.unpack('>f', struct.pack('>HH', *rr.registers))
            meter_data[reg[0]] = float_val[0]

        if address in phase_mapping:
            phase = phase_mapping[address]
            meter_data['phase_assignment'] = phase

            for fld in phase_fields:
                if fld in meter_data:
                    source_val = meter_data[fld]
                    tgt_field = "%s_%d" % (fld, phase)
                    meter_data[tgt_field] = source_val

        with open("meter%02d.csv" % address, "a") as of:
            csv_data = [str(meter_data[key]).strip() for key in meter_data]
            csv_row = "%f,%s" % (time.time(), ",".join(csv_data))
            of.write(csv_row)
            of.write("\n")

        #print("Meter %d: %s V %s W %s kWh import %s kWh export %s peak watt" % (
        #    address,
        #    meter_data['voltage'],
        #    meter_data['p_active'],
        #    meter_data['p_import'],
        #    meter_data['p_export'],
        #    meter_data['p_peak']
        #))
        print("Read meter data for meter %d" % address)

        now = datetime.utcnow().replace(tzinfo=timezone.utc)
        meter_data['meter_id'] = address
        meter_data['timestamp'] = serie_timestamp
        meter_data['actual_timestamp'] = now.isoformat()
        meter_data['latency'] = time.time() - start
        meter_data['id'] = hash("%s%s" % (address, meter_data['timestamp']))
        index_name = "eastron_data-%04d-%02d-%02d" % (now.year, now.month, now.day)

        pprint.pprint(meter_data)
        es.index(index=index_name, doc_type="reading", id=meter_data['id'], body=meter_data)

    to_sleep = next_time - time.time()
    if to_sleep > 0:
        print("Sleeping %f" % to_sleep)
        time.sleep(to_sleep)
